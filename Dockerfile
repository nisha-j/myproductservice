FROM gradle:6.9.0-jdk11-openj9 AS product-service
RUN mkdir -p /app/code
WORKDIR /app/code
COPY src /app/code/src
ADD gradlew build.gradle /app/code
RUN gradle clean build -x test
#Multilayer demo

FROM openjdk:11.0.11-jre
RUN mkdir -p /app/code
WORKDIR /app/code
COPY --from=product-service /app/code/build/libs/code-0.0.1-SNAPSHOT.jar /app/code
EXPOSE 9000
ENTRYPOINT ["java","-jar","/app/code/code-0.0.1-SNAPSHOT.jar"]
